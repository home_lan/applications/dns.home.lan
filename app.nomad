job "dns_home_lan" {
    datacenters = ["primary"]

    type = "system"

    group "pi-hole" {
        network {
            port "http" {
                to = 80
                host_network = "private"
            }

            port "dns" {
                to = 53
                static = 53
                host_network = "public"
            }
        }

        service {
            provider = "nomad"
            port = "http"
            name = "pihole-http"


            tags = [
                "traefik.enable=true",
                "traefik.http.routers.pihole.rule=Host(`dns.home.lan`)"
            ]

            check {
                type = "tcp"
                name = "Pi-hole HTTP"
                port = "http"
                interval = "5s"
                timeout  = "2s"
            }

            check_restart {
                limit = 3
                grace = "90s"
                ignore_warnings = false
            }
        }

        service {
            provider = "nomad"
            port = "dns"
            name = "pihole-dns"

            check {
                port = "dns"
                type = "tcp"
                name = "Pi-hole DNS"
                interval = "30s"
                timeout = "5s"
            }

            check_restart {
                limit = 3
                grace = "90s"
                ignore_warnings = false
            }
        }

        task "run" {
            driver = "docker"

            config {
                image = "pihole/pihole:2023.03.1"
                network_mode = "bridge"

                ports = ["http", "dns"]

                volumes = [
                    "/opt/nomad/storage/dns.home.lan/etc:/etc/pihole",
                    "/opt/nomad/storage/dns.home.lan/dnsmasq.d:/etc/dnsmasq.d"
                ]
            }

            template {
                destination = "${NOMAD_SECRETS_DIR}/env.vars"
                env = true
                data = <<EOT
                TZ = "America/Los_Angeles"
                WEBPASSWORD = "{{with nomadVar "nomad/jobs/dns_home_lan" }}{{ .WEBPASSWORD }}{{ end }}"
                EOT
            }
        }
    }
}
